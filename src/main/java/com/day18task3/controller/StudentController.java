package com.day18task3.controller;

import com.day18task3.model.Student;
import com.day18task3.repository.StudentRepository;
import com.day18task3.repository.StudentSubjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {
    // Spring Boot Logger
    public static final Logger log = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    StudentSubjectRepository studentSubjectRepository;

    // -----------------Register Student----------------------
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResponseEntity<Student> insertStudent(@RequestBody Student student) {
        log.info("Registering...{}", student.getName());
        int result = studentRepository.insert(student);

        if (result > 0) {
            log.info("Register {} success!", student.getName());
            return new ResponseEntity<Student>(student, HttpStatus.CREATED);
        } else {
            log.info("Register {} failed!", student.getName());
            return new ResponseEntity<Student>(student, HttpStatus.CONFLICT);
        }
    }

    // -----------------Get Students Count-----------------------
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public ResponseEntity<?> getCount() {
        log.info("Counting Students...");
        int countResult = studentRepository.count();
        log.info("[COUNT] Total students: {}", countResult);
        return new ResponseEntity<Integer>(countResult, HttpStatus.OK);
    }

    // ------------------Get All Students-----------------------
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Student>> getAllStudents() {
        log.info("Finding All Students...");
        List<Student> students = studentRepository.findAll();
        log.info("[FIND_ALL] {}", students);
        return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
    }

    // -----------------Get Student by ID-----------------------
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Student> getStudentById(@PathVariable("id") int id) {
        log.info("[FIND_BY_ID] :{}", id);
        Student student = studentRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        log.info("{}", student);
        return new ResponseEntity<Student>(student, HttpStatus.OK);
    }

    // -----------------Update Attendance---------------------
    @RequestMapping(value = "/update/attendance/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> updateAttendance(@PathVariable("id") int id) {
        log.info("Updating Attendance, Id :{}", id);
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("YYYY-MM-dd");
        String todayDate = now.format(dtf);

        Student student = studentRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        log.info("{}", student);
        String attendance = student.getAttendance();

        if (attendance.equals(todayDate)) {
            return new ResponseEntity<String>("already attend today", HttpStatus.CONFLICT);
        } else {
            student.setAttendance(todayDate);
            studentRepository.updateAttendance(id, todayDate);
            return new ResponseEntity<String>("success", HttpStatus.OK);
        }
    }

    // -------------------Delete Student------------------------
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteStudentById(@PathVariable("id") int id) {
        log.info("[DELETE] :{}", id);
        int result = studentRepository.deleteById(id);
        log.info("rows affected: {}", result);
        return new ResponseEntity<String>("success", HttpStatus.OK);
    }
}
