package com.day18task3.model;

public class StudentSubject {
    private int id;

    private int studentid;

    private int subjectid;

    private String attendance;

    private int score;

    public StudentSubject(int id, int studentid, int subjectid, String attendance, int score) {
        this.id = id;
        this.studentid = studentid;
        this.subjectid = subjectid;
        this.attendance = attendance;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudentid() {
        return studentid;
    }

    public void setStudentid(int studentid) {
        this.studentid = studentid;
    }

    public int getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(int subjectid) {
        this.subjectid = subjectid;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
